# SRi
The **S**cape**R**une **i**nclude is a library of routines that targets a [specific RuneScape private server](https://www.scaperune.net/).

Read [USAGE.md](USAGE.md) for installation, setup, and usage instructions. Check out [TODO.md](TODO.md) for a list of outstanding issues that need addressed.

## Requirements
* [Simba](https://github.com/Villavu/Simba): two versions of Simba are known to be compatible with SRi. The **stable** version is recommended as it retains compatibility with [SRL-Development](https://github.com/KeepBotting/SRL-Development) but either will work.
  * [Simba 1.4 (stable)](https://github.com/Villavu/Simba/releases/tag/simba1400-release)
  * [Simba 1.4 (legacy)](https://github.com/ollydev/Simba/releases/tag/autobuild-simba1400)
* [SRL-Fonts](https://github.com/SRL/SRL-Fonts) must also be installed.

## Notes
We use a legacy version ("[v0](https://github.com/ollydev/SimpleOCR/releases/tag/v0)") of [SimpleOCR](https://github.com/ollydev/SimpleOCR) which corresponds to [this commit](https://github.com/ollydev/SimpleOCR/commit/f5e8497a63f3f112cd398d6ee7dcda0ed0c658fc). The plugin is part of the include so it does **not** need installed separately, and will not conflict with other installed versions.

The [SMART 8.6](https://github.com/BenLand100/SMART/releases/tag/v8.6) plugin was previously utilized, up until [this commit](https://gitlab.com/branon/SRi/-/commit/0e23722ee680502042b584ddc54f6af5024e8f45).

## Credits
The following people have contributed code directly to SRi:
* [Olly](https://github.com/ollydev)
* [Saml1991](https://github.com/saml1991)

SRi uses lots of [SRL](https://github.com/SRL) code (see also [Villavu](https://github.com/Villavu)). Notable projects from which code has been taken, in modified form or reproduced identically, are:
* [SRL](https://github.com/SRL/SRL) 
* [SRL-OSR](https://github.com/SRL/SRL-OSR) 
* [SRL-6](https://github.com/SRL/SRL-6)
* [AeroLib](https://github.com/J-Flight/AeroLib)
