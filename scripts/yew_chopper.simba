// standard treeDTM := DTMFromString('mWAAAAHicY2FgYFjMBMFLgXghEGcCxXIYIDQIawbIMARaKDIYJSuB2SJAMVYkzIiGQQAAqLIG8g==');
// darker tree, unique to icon of these two yews on mm. lower tol DTM := DTMFromString('mWAAAAHicY2FgYIhgYmAIA+IQII4C4mygWD4QZ0Fpo2QlBmsDEQYFTUkwZgWKIWNGNAwCADV6BS4=');
// mm symbol DTM := DTMFromString('mbQAAAHicY2VgYEhiYmCIZoLQmVDaggGCDaG0PRD///+fgZmZHwX/B4qzomFGLBgMABSKB3s=');
// bank symbol DTM := DTMFromString('mbQAAAHicY2VgYGhmQuA2IG4AYkOguDYQmwOxHhCbAPGbK1EM2xYHMFSkC4IxiM0KFEfHjFgwGAAA7V4K/g==');

program seers_yews;
{$include_once SRi/core/client.simba}
{$include_once SRi/utilities/walker.simba}

var
  c: TRSClient;
  player: TRSPlayer;
  viewport: TRSViewport;
  minimap: TRSMinimap;
  yewTree, banker: TColorInfo;
  mouse: TMouse;
  option_bank_banker, option_store_all, logsDTM, x, y: integer;
  bankPoint: TPoint  = [191, 279];
  treesPoint: TPoint = [267, 159];
  path, path2: TPointArray;

const
  STATE_UNKNOWN:   integer = -1;
  STATE_CHOP:      integer =  0;
  STATE_BANK:      integer =  1;
  STATE_WALKTREES: integer =  2;
  STATE_WALKBANK:  integer = 3;

// Overload: returns the distance between two TPoints
function distance(a, b: TPoint): integer; overload;
begin
  exit(distance(a.x, a.y, b.x, b.y));
end;

// clicks a tree, returns true if we (red)clicked
function chop(): boolean;
begin
  if viewport.findObj(yewTree, ['Chop', 'down', 'Yew']) then
  // 12 second cycles, 85 second max (autologout at 90s), exit when full
  player.getInventory().waitShift(true, true, 12, 80);
end;

// banks the inventory, returns true if the bankscreen is up and the inventory is full
function bank(): boolean;
begin
  banker.create(6524090, 12, 0.03, 0.97, viewport.getBounds());

  option_bank_banker := BitmapFromString(79, 11, 'meJztVFsSgCAI7HZdoN/ufwybcj' +
        'QRVhCcvmL8YBBhVx4pEdmP03fU51tK7XEnijzPILdHInxrHIVsyeQ' +
        'GXOMEQaqAl5ANAv7JToGskruMXyF/ThbNQjezbSJxltGMv8+xPzKi' +
        'mR3bOS/0YwJIilOxM17ij5mCFMBiG/Oii5/Q2hHTcRvzoov4iV3sD' +
        'Ra880Rkx6SQjnrYiEf1MfY8r3jXrqt0xHeCiE2HswAqbl9QDuK+Be' +
        'UgPlhoHdnbQoUglK5m9c8SqUEu+gNcOQ==');

  option_store_all := BitmapFromString(56, 11, 'meJy9VFsOwCAI43ZcYL/e/xhuMZ' +
        'EQC+h8kX1og1BbJj+Jn5RrcNle+8y+GsE1EXkJR0k2fTWOOVTiw2W' +
        'hk4+S1O0Cnp5013iOcGvSzMlBR6ZBr75ElydS1b40R8xJzmAiZnq+' +
        '6/Hr8gyGZNCaronrvo/U/2sN6rzCUzT0vJvmSSq2/EfexQU3hYpfs' +
        'EbSLTy5Ppt4ccRNleKznqRBEVy/ct1x/g==');

  logsDTM:= DTMFromString('mFQEAAHic42VgYChhYmAoAuJaIG4E4mQgzgTidCCuAeJSIK4E4lwgTgHiBiB+DtT3HojfMEDYL4H4LRA/AOJHQPwOyv8IVfMaiMMcOIEkI0n4PwPpgDQbIBgFAADekhOV');

  viewport.findObj(banker, ['Bank'], MOUSE_RIGHT);
  wait(500);
  FindBitmap(option_bank_banker, x, y);
  mouse.move(x, y, MOUSE_LEFT);

  wait(2000);

  mouse.dtm(logsDTM, player.getInventory().getBounds(), MOUSE_RIGHT);
  wait(500);
  FindBitmap(option_store_all, x, y);
  mouse.move(x, y, MOUSE_LEFT);
  wait(1000);
end;

// walks a path, optionally inverts the path to walk in the opposite direction
function travel(path: TPointArray; invert: boolean = false): boolean;
begin
  c.minimap.polish(10000, 15);
  walker.Debug();
    Walker.WalkPath(path);
end;

// Determines the finite state based on the status of the game window
function getState(): integer;
var
  pos: TPoint;
begin
  result := -1; // use -1 as an invalid state

  pos := Walker.GetPosition();
  Walker.Debug();

  // if the inventory is full and we are far away from the bank, we need to walk
  if ((player.getInventory().isFull()) and (distance(pos, bankPoint) > 30)) then
  begin
    writeLn('Inv full, far away from bank, we need to walk to the bank');
    result := STATE_WALKBANK;
  end;

  // if the inventory is NOT full and we are far away from the trees, we need to walk
  if (not (player.getInventory().isFull()) and (distance(pos, treesPoint) > 30)) then
  begin
    writeLn('Inv not full, and far away from trees, we need to walk to the trees');
    result := STATE_WALKTREES;
  end;

  // if the inventory is NOT full and we are close to the trees, cut one down!
  if (not (player.getInventory().isFull()) and (distance(pos, treesPoint) < 30)) then
  begin
    writeLn('Inv not full, and close to trees, we need to cut a tree');
    result := STATE_CHOP;
  end;

  // if the inventory is full and we're close to the bank, drop stuff off!
  if ((player.getInventory().isFull()) and (distance(pos, bankPoint) < 30)) then
  begin
    writeLn('Inv full, and close to bank, we need to use the bank');
    result := STATE_BANK;
  end;

  writeLn('The state is: ' + toStr(result));
end;

// contains the steps required to place the game into a predefined state
// attempts to play the game until the state has been reached, then exits
procedure setState(state: integer);
begin
  case state of
    STATE_CHOP: chop();
    STATE_BANK: bank();
    STATE_WALKTREES: travel(path2);
    STATE_WALKBANK:  travel(path);
  end;
end;

begin
  treesPoint := [191, 279];
  bankPoint := [267, 159];
  yewTree.create(3965297, 16, 0.10, 0.58, viewport.getBounds());

  // from trees to bank
  path := [[213, 281], [255, 286], [255, 243], [268, 190], [268, 157]];

  // from bank to trees
  path2 := [[268, 186], [258, 232], [240, 259], [192, 280]];

  Walker.Init(IncludePath + 'SRi/maps/seers.png', 1);

  if (not c.load()) then
  begin
    writeLn('Could not find a client after 30 seconds, terminating');
    terminateScript();
  end;

  c.activate();
  c.waitReady();

  //player.load('player1');

  if (not player.isLoggedIn()) then
  begin
    c.login(player);
    c.viewport.setAngle();
    c.minimap.align(0);
    c.chatbox.setPrivate(0);
    writeLn('Ready player one');
  end;
  repeat
  setState(getState());
  wait(100);
  until(not(player.isLoggedIn()));


end.

