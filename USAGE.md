# Installation guide

SRi is only tested on the following targets. 32-bit targets of any type are not supported.

* Windows, 64-bit x86
* Linux, 64-bit x86 and 64-bit ARM

## Windows package

Installing on Windows is best accomplished by using a package which bundles together all required components, including Java. 

Download the [current release](https://branon.me/files/SRi-Windows-Package_20210130.zip) and unzip the top-level directory somewhere sensible.

Once extracted, navigate into the directory and run `Simba-Win64.exe`. Refer to the [beginner's tutorial](https://villavu.com/forum/showthread.php?t=58935) if you're unsure how to use the IDE.

When you are ready to bot, move on to the next section.

## Client configuration

SRi only supports the 462 gameframe. Your launcher options must be configured thus:

![](https://gitlab.com/branon/SRi/-/raw/master/bmp/doc1.png)

High detail is recommended but low detail should be okay too. "Do not display again" is optional, but if you don't check it, SRi won't be able to auto-launch the game client.

Recommended settings:

![](https://gitlab.com/branon/SRi/-/raw/master/bmp/doc2.png)

Max brightness is the only real requirement here, the rest is preference.

## Running scripts

The default script skeleton is generally located at [`Includes/SRi/scripts/skel.simba`](https://gitlab.com/branon/SRi/-/blob/master/scripts/skel.simba) within Simba's installation directory.

Running the script skeleton will automatically launch and target a ScapeRune client, before attempting to log in with some (hopefully bogus) credentials. 

Replace the credentials with your own to observe a complete login, including some basic interaction tasks after the player is logged in. Keep an eye on Simba's debug box for script output!

Auto-launching the client is supported cross-platform, but is only supported on Windows when using the Windows package with bundled Java.

Please peruse SRi's `scripts/` subdirectory for existing public scripts. No warranties!

## Storing player credentials

To avoid accidentally leaking passwords, you have the option of storing player credentials in `Includes/SRi/credentials.ini`.

Create `credentials.ini` in the root of SRi's directory, and populate it with the following syntax:

```
[nickname]
username=MyUsername
password=MyPassword

[bob]
username=tehnoobshow
password=secret123
```

In a script, use `TRSPlayer.load('nickname');` to load player credentials and create your player before logging in.

### Manual installation (Linux)

Here is a quick rundown on how to assemble an environment capable of running scripts written for SRi. This process is used to create the Windows package.

#### Step 0
Install required programs

SRi requires a JRE because it makes heavy use of a Java-based RuneScape client. Ensure you have one intalled, OpenJDK's JRE works fine. The ScapeRune client is known to run under Java 8 or 11.

SRi also requires [Simba](http://wizzup.org/simba/) and currently uses a legacy release of [Simba 1400](https://github.com/ollydev/Simba/releases/tag/autobuild-simba1400). 

#### Step 1
Install required plugins and fonts

A legacy release of the [SimpleOCR](https://github.com/ollydev/SimpleOCR/releases/tag/v0) plugin is required. Drop the binary that matches your OS/architecture into Simba's `Plugins/` directory.

You will also need [SRL-Fonts](https://github.com/SRL/SRL-Fonts). Use Simba's package manager to install the fonts, or clone the repository from inside Simba's `Fonts/` directory.

#### Step 2
Install the include itself

Running `git clone https://gitlab.com/branon/SRi` from inside Simba's `Includes/` directory is the only recommended way to do this.

The final directory structure must match the following:

```
Includes/
└── SRi/
    ├── core/
    ├── interfaces/
    ├── resources/
    ├── utilities/
    └── [...]
```

Updating the include is easy: simply `git pull`.
